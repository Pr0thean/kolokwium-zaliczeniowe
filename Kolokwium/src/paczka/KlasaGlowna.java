package paczka;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class KlasaGlowna {

	private JFrame frame;
	private JPanel panel, panel_1, panel_2;
	

	public KlasaGlowna() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 742, 507);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panel = new Panel1();
		frame.getContentPane().add(panel);
		
		panel_1 = new Panel2();
		frame.getContentPane().add(panel_1);
		
		panel_2 = new Panel3();
		frame.getContentPane().add(panel_2);
	}
	
	class Panel1 extends JPanel implements ActionListener {
		
		Timer t = new Timer(3, this);	
		int x = 650, y = 40, velX = 2, velY = 0;
		/*int xPoints[] = {590, 620, 620, 650, 650};
		int yPoints[] = {135, 135, 11, 11, 135};*/
		
		public Panel1() {
	    	setBounds(10, 11, 700, 140);
	    	setBorder(BorderFactory.createLineBorder(Color.BLUE));
	    }

	    @Override
	    public void paintComponent(Graphics g) {
	        g.drawRect(x, y, 50, 100);
	        t.start();
	    }
	    
	    public void actionPerformed(ActionEvent e) {
	    	x -= velX;
	    	y -= velY;
	    	repaint();
	    }
	}
	
	class Panel2 extends JPanel {
	    public Panel2() {
	    	setBounds(10, 162, 700, 140);
	    	setBorder(BorderFactory.createLineBorder(Color.BLUE));
	    }

	    @Override
	    public void paintComponent(Graphics g) {

	    }
	}
	
	class Panel3 extends JPanel {
	    public Panel3() {
	    	setBounds(10, 313, 700, 140);
	    	setBorder(BorderFactory.createLineBorder(Color.RED));
	    }

	    @Override
	    public void paintComponent(Graphics g) {

	    }
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KlasaGlowna window = new KlasaGlowna();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
